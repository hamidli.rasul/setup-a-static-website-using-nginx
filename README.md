# Setup a Static Website Using Nginx



## Getting started

	Key Concepts Covered

1. DNS
2. Linux
3. Webserver
4. Nginx
5. Dig command
5. SSL (Letsencrypt)
6. OpenSSL command.

**1. Buy a domain name from a domain Registrar**

I have bought the domain (rasulsdevopsprojects.info) from GoDaddy as a first step:

![Alt text](images/domain_name_purchase.png)

**2. Setup ubuntu server in AWS**

1. Ubuntu 20.04 LTS t2.micro (with key pair for easily connecting and working later!)
1. 10GB volume
1. With new security group
1. HTTP, HTTPS, SSH enabled

![Alt text](images/ec2_instance.png)

**3. SSH into the server and install Nginx**

~~~~
sudo apt-get -y update
sudo apt-get -y install nginx
sudo service nginx start
sudo service nginx enabled
sudo service nginx status
~~~~

**4. Download freely available HTML website files**

Downloaded website template by #wget to /var/www/html nginx website directory and unzip it

~~~~
wget https://www.free-css.com/free-css-templates/page201/cyber-tech

sudo apt-get install unzip

unzip cyber-tech.zip -d /var/www/html
~~~~

**5. Validate the website using the server IP address**

I have validated with public IP of EC2 instance and it openned successfully, then I have attached Elastic IP to my EC2 instance

![Alt text](images/elastic_ip.png)

-----------------------------------

![Alt text](images/ec2_instance.png)

**6. In GoDaddy DNS account, create an A record and add the Elastic IP**

I have added A record with my Elastic IP address:

![Alt text](images/adding_a_record_on_dns.png)

**7. Use the dig command to verify the DNS records.**

~~~~
dig rasulsdevopsprojects.info
~~~~

![Alt text](images/dns_record_with_dig_command.png)

8. **Using DNS verify the website setup** (when I take snapshot below, I have already configured the HTTPS too which it will on the next steps of the project, that`s why it shows with HTTPS 🙂)

![Alt text](images/verify_website_setup.png)

**9. Create a Letsencryp certificate for the DNS and configure it on the Nginx server**

Installed Certbot:
~~~~
sudo apt-get install certbot python3-certbot-nginx
~~~~
Generate certificates:
~~~~
sudo certbot --nginx -d rasulsdevopsprojects.info -d www.rasulsdevopsprojects.info
~~~~
Following the prompts and typed my personal email, agree terms and conditions and chose the redirect HTTP requests forward to the HTTPS too

Tested automatic renewal: 
~~~~
sudo certbot renew --dry-run
~~~~

**10. Validate the website SSL using the OpenSSL utility**

Installed the OpenSSL:
~~~~
sudo apt-get install openssl
~~~~
Check the SSL certificate: 
~~~~
openssl s_client -connect rasulsdevopsprojects.info:443
~~~~

Verify the certificate (normally it`s longer than what is on the picture below 🙂): 

![Alt text](images/validate_ssl.png)
![Alt text](images/validate_ssl_2.png)
